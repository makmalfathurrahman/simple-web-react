import { Link } from 'react-router-dom'

import Footer from '../components/Footer'
import NavigationBar from '../components/NavigationBar'
import '../styles/Home.css'

import Monas from '../assets/monas.jpg'

const Home = () => {
  return (
    <>
      <NavigationBar />
      <div className="home" style={{ backgroundImage: `url(${Monas})` }}>
        <div className="headerContainer">
          <h1> Tour Travel </h1>
          <p> LET&apos;S EXPLORE INDONESIA</p>
          <Link to="/trip">
            <button> READ MORE </button>
          </Link>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Home
