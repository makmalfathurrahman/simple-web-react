import Footer from '../components/Footer'
import NavigationBar from '../components/NavigationBar'
import '../styles/Contact.css'

import Borobudur from '../assets/borobudur.jpg'

const Contact = () => {
  return (
    <>
      <NavigationBar />
      <div className="contact">
        <div
          className="leftSide"
          style={{ backgroundImage: `url(${Borobudur})` }}
        ></div>
        <div className="rightSide">
          <h1> Contact Us</h1>

          <form id="contact-form" method="POST">
            <label htmlFor="name">Full Name</label>
            <input name="name" placeholder="Enter full name..." type="text" />
            <label htmlFor="email">Email</label>
            <input name="email" placeholder="Enter email..." type="email" />
            <label htmlFor="message">Message</label>
            <textarea
              rows="6"
              placeholder="Enter message..."
              name="message"
              required
            ></textarea>
            <button type="submit"> Send Message</button>
          </form>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Contact
