import { TripList } from '../helpers/TripList'
import TripItem from '../components/TripItem'

import '../styles/Trip.css'
import NavigationBar from '../components/NavigationBar'
import Footer from '../components/Footer'

const Trip = () => {
  return (
    <>
      <NavigationBar />
      <div className="trip">
        <h1 className="tripTitle">Our Trip Catalog</h1>
        <div className="tripList">
          {TripList.map((tripItem, key) => {
            return (
              <TripItem
                key={key}
                image={tripItem.image}
                name={tripItem.name}
                description={tripItem.description}
              />
            )
          })}
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Trip
