import Bandung from '../assets/bandung.jpg'
import Semarang from '../assets/semarang.jpeg'
import Yogyakarta from '../assets/yogyakarta.jpg'
import Surabaya from '../assets/surabaya.jpg'
import Bali from '../assets/bali.jpg'
import LabuanBajo from '../assets/labuanbajo.jpg'

export const TripList = [
  {
    name: 'Bandung',
    image: Bandung,
    description:
      "The capital city of the Indonesian province of West Java. Bandung is the country's third-largest metropolitan area, with nearly nine million inhabitants"
  },
  {
    name: 'Semarang',
    image: Semarang,
    description:
      "The capital and largest city of Central Java province in Indonesia. The population of the city was 1,653,524 at the 2020 census, making it Indonesia's ninth most populous city"
  },
  {
    name: 'Yogyakarta',
    image: Yogyakarta,
    description:
      'The capital city of Special Region of Yogyakarta in Indonesia, in the south-central part of the island of Java. As the only Indonesian royal city still ruled by a monarchy'
  },
  {
    name: 'Surabaya',
    image: Surabaya,
    description:
      'The capital city of the Indonesian province of East Java and the second-largest city in Indonesia. Located on the northeastern border of Java island, on the Madura Strait'
  },
  {
    name: 'Bali',
    image: Bali,
    description:
      'A province of Indonesia and the westernmost of the Lesser Sunda Islands. East of Java and west of Lombok, the province includes the island of Bali and a few smaller neighbouring islands'
  },
  {
    name: 'Labuan Bajo',
    image: LabuanBajo,
    description:
      'A fishing town located at the western end of the large island of Flores in the Nusa Tenggara region of east Indonesia. It is the capital of the West Manggarai Regency'
  }
]
