import { BsInstagram, BsTwitter, BsFacebook, BsLinkedin } from 'react-icons/bs'
import '../styles/Footer.css'

function Footer() {
  return (
    <div className="footer">
      <div className="socialMedia">
        <BsInstagram /> <BsTwitter /> <BsFacebook /> <BsLinkedin />
      </div>
      <p> &copy; 2023 Tour Travel</p>
    </div>
  )
}

export default Footer
