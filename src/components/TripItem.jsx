// eslint-disable-next-line react/prop-types
function MenuItem({ image, name, description }) {
  return (
    <div className="tripItem">
      <div style={{ backgroundImage: `url(${image})` }}> </div>
      <h1> {name} </h1>
      <p> {description} </p>
    </div>
  )
}

export default MenuItem
